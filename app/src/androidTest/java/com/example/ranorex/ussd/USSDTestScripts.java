package com.example.ranorex.ussd;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Ranorex on 9/20/2016.
 */
@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class USSDTestScripts {

    protected UiDevice device;

    @Before
    public void beforeTest() throws Exception {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.pressRecentApps();
        device.wait(Until.hasObject(By.clazz("android.widget.Button")), 6000);
        closeAllApps();
        device.wait(Until.hasObject(By.text("Apps")), 3000);
        UiObject2 phoneButton = device.findObject(By.desc("Phone"));
        phoneButton.click();
        device.wait(Until.hasObject(By.text("8")), 3000);
    }

    private void closeAllApps() throws InterruptedException {
        Thread.sleep(3000);
        List<UiObject2> closeBtns = device.findObjects(By.clazz("android.widget.Button"));
        if (closeBtns.size() == 0){
            device.pressHome();
        } else {
            UiObject2 closeAll = device.findObject(By.clazz("android.widget.Button"));
            closeAll.click();
        }
    }

    private boolean matchString(String value, String matchingValue){
        return value.equals(matchingValue);
    }

    private void typeUSSD(String ussdCode){
        char[] ussdAsArray = ussdCode.toCharArray();
        int size = ussdAsArray.length;
        for (int x = 0; x < size ; x++){
            char value = ussdAsArray[x];
            String sign = String.valueOf(value);
            pressButton(sign);
        }
    }

    private void pressButton(String button){
        switch (button){
            case "*":
                UiObject2 asterisk = device.findObject(By.desc("Asterisk,Tap and hold with two fingers to add pause."));
                asterisk.click();
                return;
            case "#":
                UiObject2 pound = device.findObject(By.text("#"));
                pound.click();
                return;
            case "0":
                UiObject2 zero = device.findObject(By.text("0"));
                zero.click();
                return;
            case "1":
                UiObject2 one = device.findObject(By.text("1"));
                one.click();
                return;
            case "2":
                UiObject2 two = device.findObject(By.text("2"));
                two.click();
                return;
            case "3":
                UiObject2 three = device.findObject(By.text("3"));
                three.click();
                return;
            case "4":
                UiObject2 four = device.findObject(By.text("4"));
                four.click();
                return;
            case "5":
                UiObject2 five = device.findObject(By.text("5"));
                five.click();
                return;
            case "6":
                UiObject2 six = device.findObject(By.text("6"));
                six.click();
                return;
            case "7":
                UiObject2 seven = device.findObject(By.text("7"));
                seven.click();
                return;
            case "8":
                UiObject2 eight = device.findObject(By.text("8"));
                eight.click();
                return;
            case "9":
                UiObject2 nine = device.findObject(By.text("9"));
                nine.click();
                return;
            default:
                return;
        }
    }

    private String[] ussdMenuToArray(String ussdMenu){
        String[] returnArray = ussdMenu.split("/", -1);
        return returnArray;
    }

    private boolean validateFirstCharacter(String menuItem){
        char character = menuItem.charAt(0);
        boolean isLetter = (character >= '0' && character <= '9'|| character == 'q');
        return isLetter;
    }

    private void ussdInteration(String ussdMenu) throws Exception {
        String[] ussdArray = ussdMenuToArray(ussdMenu);
        int arraySize = ussdArray.length;
        for (int x = 0; x < arraySize; x++){
            boolean firstCharacter = validateFirstCharacter(ussdArray[x]);
            if (firstCharacter){
                sendUSSDOption(ussdArray[x]);
            } else {
                sendUSSDMenu(ussdArray[x]);
            }
        }
    }

    private void sendUSSDMenu(String menuItem) throws Exception {
        String menuID = null;
        switch (menuItem){
            case "Send_Money":
                menuID = "1";
                break;
            case "Enter_Phone_No":
                menuID = "1";
                break;
            case "Use_Beneficiary":
                menuID = "2";
                break;
            case "Select_Beneficiary":
                menuID="1";
                break;
            case "Buy_Airtime":
                menuID = "29";
                break;
            case "My_Phone":
                menuID = "1";
                break;
            case "Customer_Create_Account":
                menuID = "55";
                break;
            case "Ok":
                menuID = "1";
                break;
            case "Remark":
                menuID = "TestRemark";
                break;
            case "Purchase_Airtime":
                menuID = "20";
                break;
            case "Organization_Etopup":
                menuID = "1";
                break;
            case "TZS":
                menuID = "1";
                break;
            case "OTC_Service":
                menuID = "14";
                break;
            case "Perform_OTC_Service":
                menuID = "2";
                break;
            case "Pay_Bill_OTC":
                menuID = "2";
                break;
            case "Business_Withdraw":
                menuID = "9";
                break;
            case "Initiates_Business_Withdraw":
                menuID = "1";
                break;
            case "Short_Code":
                menuID = "2";
                break;
            case "Lipa_Na_MPESA":
                menuID = "5";
                break;
            case "Buy_Goods_And_Services":
                menuID = "2";
                break;
            case "Default_Account":
                menuID = "1";
                break;
            case "Group":
                menuID="61";
                break;
            case "Transactions_Group":
                menuID="1";
                break;
            case "Group_transfer_to_group_member":
                menuID="2";
                break;
            case "Enter_Group_code":
                menuID="1";
                break;
            case "Service_Purchase":
                menuID = "22";
                break;
            case "Supercheka":
                menuID = "2";
                break;
            case "Other_Phone":
                menuID = "2";
                break;
            case "Group_P2G_Transfer":
                menuID = "1";
                break;
            case "Group_Transfer_to_Group":
                menuID = "4";
                break;
            case "Two_Part_Payment":
                menuID = "74";
                break;
            case "Two_Part_Buy_Goods":
                menuID = "1";
                break;
            default:
                break;
        }
        Thread.sleep(2000);
        UiObject2 ussdTextArea = device.findObject(By.clazz("android.widget.EditText"));
        ussdTextArea.setText(menuID);
        UiObject2 ussdSend = device.findObject(By.text("Send"));
        ussdSend.click();
        device.wait(Until.hasObject(By.text("Send")), 18000);
    }

    private void sendUSSDOption(String optionItem){
        device.wait(Until.hasObject(By.clazz("android.widget.EditText")), 18000);
        UiObject2 ussdTextArea = device.findObject(By.clazz("android.widget.EditText"));
        ussdTextArea.setText(optionItem);
        UiObject2 ussdSend = device.findObject(By.text("Send"));
        ussdSend.click();
        device.wait(Until.hasObject(By.clazz("android.widget.TextView")), 18000);
    }

    private String getUSSDFinalScreen(){
        UiObject2 msgTextArea = device.findObject(By.clazz("android.widget.TextView"));
        String text = msgTextArea.getText();
        return text;
    }

    private void closeUSSDTray() throws InterruptedException {
        Thread.sleep(2000);
        device.pressBack();
        List<UiObject2> tray = device.findObjects(By.clazz("android.widget.TextView"));
        if (tray.size() == 0){
            device.pressHome();
        } else {
            device.pressBack();
        }
    }

    private void writeToTextFile(String texts){
        try {
            String fpath = "/sdcard/tsxt_USSD.txt";
            File file = new File(fpath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("USSD_INFO - " + texts);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArrayList getUnreadMsgs(){
        Uri SMS_INBOX = Uri.parse("content://sms/inbox");
        Cursor cursor = InstrumentationRegistry.getTargetContext().getContentResolver().query(SMS_INBOX, null, "read=0", null, null);
        ArrayList sms = new ArrayList();
        while (cursor.moveToNext()) {
            String address = cursor.getString(cursor.getColumnIndex("address"));
            long smsTimestamp = Long.parseLong(cursor.getString(cursor.getColumnIndex("date")));
            long systemTimestamp = System.currentTimeMillis();
            if (address.equals("FE3") && (systemTimestamp - smsTimestamp) < 90000){
                String body = cursor.getString(cursor.getColumnIndex("body"));
                sms.add("USSD_SMS -> " + address + " <--> " + body);
            }
        }
        return sms;
    }

    private void writeUSSDSms(ArrayList unreadSMS){
        try {
            String fpath = "/sdcard/tsxt_SMS.txt";
            File file = new File(fpath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int x = 0; x < unreadSMS.size(); x++){
                bufferedWriter.write("USSD_SMS - " + unreadSMS.get(x));
                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public  void testRunPhone() throws Exception {
        Bundle argumentParam = InstrumentationRegistry.getArguments();
       // String ussdCode = "*200*602#";
       // String ussdMenu = "Group/Transactions_Group/Group_transfer_to_group_member/Enter_Group_code/000018/40720332295/100/Remark/8888/Remark/1";
        String ussdCode =null;
        String ussdMenu =null;
        if ( argumentParam != null ) {
           System.out.println("ARGUMENT_FOUNDs");
            ussdCode = argumentParam.getString("ussdCode");
            ussdMenu = argumentParam.getString("ussdMenu");
        }else{
            System.out.println("NO_ARGUMENT_FOUND");
        }
        typeUSSD(ussdCode);
        UiObject2 digitsTextArea = device.findObject(By.clazz("android.widget.EditText"));
        assertTrue("USSD code mismatch with the dial area value", matchString(digitsTextArea.getText(), ussdCode));
        UiObject2 dialButton = device.findObject(By.desc("Call"));
        dialButton.click();
        device.wait(Until.hasObject(By.text("Send")), 18000);
        ussdInteration(ussdMenu);
        device.wait(Until.hasObject(By.clazz("android.widget.TextView")), 18000);
        String ussdMsg = getUSSDFinalScreen();
        writeToTextFile(ussdMsg);
        closeUSSDTray();
        Thread.sleep(30000);
        device.pressHome();
        ArrayList unreadSMS = getUnreadMsgs();
        writeUSSDSms(unreadSMS);
    }
}
